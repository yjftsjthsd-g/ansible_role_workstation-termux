workstation-termux
=========

Configures platform-dependent stuff to set up termux as a workable workstation environment. This involves a touch of hackery to make Termux and Ansible even work together, horrible package installation, and some tweaking to make things play nice.


Why this is terrible
--------------------

On a "normal" system, ansible will nicely integrate with the local package
manager, say apt, and do nice things like diff requested packages against
installed packages to see what needs to be installed. Unfortunately, in spite of
a significant amount of effort, I cannot make this work against Termux. In
fairness, Ansible probably sees "apt" and assumes "Debian", which means that
Termux, with its wildly non-Unix paths and non-root operations, is going to
break a bunch of otherwise sane assumptions. I think there might also be a
missing library or something. It's been a while.

Anyways, the result of this is that this role just shells out and executes apt
commands directly, and as such has no nice diffs and no ability to skip
installing packages that are already present. It works, but it is *not* pretty.


Requirements
------------

This role must be executed over SSH from a non-Termux system, as Ansible does
*not* want to work inside Termux (that is, Termux can be an Ansible target, but
not the control machine). You will have to set up SSH connectivity from your
control machine to the Termux system yourself (preferably with keys). Also, the
usual requirement to install Python applies (since I've not worked in the raw
part to have Ansible bootstrap its own requirements).


Example Playbook
----------------

```

- name: apply configuration to termux
  hosts: termux
    roles:
      - workstation-termux
      #- workstation-user  # once the base is installed, dotfiles et al. proceed as usual
```


License
-------

GPL-2.0-or-later


